+++
title = "IRC Transport"
+++

[IRC](https://en.wikipedia.org/wiki/Internet_Relay_Chat) is a kind of chat protocol.
It is focused towards group communication, yet allows one-to-one communication.
Unlike Jabber, IRC users of one network/server can only communicate with channels and nicks of that network/server.
In IRC realm, channels are group-chats (like MUC in Jabber) and nicks are user-identifiers (like JIDs in Jabber).
To learn more about IRC, please visit [IRC-Help](https://www.irchelp.org/).

Here, you'll see how to connect to IRC from your Jabber account, via an IRC transport for Jabber, called [Biboumi](https://biboumi.louiz.org/).

**TIP!** Anyone can host a transport on their server: if you want to setup Biboumi on your server, please refer to [Self-Host](TODO).

## Selection {#selection}

### Finding an IRC network

There are many IRC networks [available](https://www.irchelp.org/networks/). Some of the popular IRC networks are:
- `irc.freenode.net` by [Freenode](https://freenode.net/)
- `irc.oftc.net` by [OFTC](https://oftc.net/)

**TIP!** You can register your nick on a network (please follow guide on respective network's site), as some channels requires you to do so.

### Finding an IRC transport

Some instances are private, while others are public.
Private instances are only accessible from accounts on the same server, while public instances are accessible from any account.

**CAUTION!** Please be aware that some IRC networks may ban public transports in order to fight spam.

#### Public

- `irc.jabber.hot-chilli.net` by [Hot-Chilli](https://jabber.hot-chilli.net)
- `irc.cheogram.com` by [Sopranica](https://soprani.ca/)

**NOTE!** Hot-Chilli requires you to [pre-register](https://jabber.hot-chilli.net/forms/preregister/) your JID, before using it's transports.

#### Private

- `irc.disroot.org` by [Disroot](https://disroot.org)
- `irc.snopyta.org` by [Snopyta](https://snopyta.org)

## Connection {#connection}

You need to tell the transport both the network you'd like to connect on, and the channel or nick you'd like to chat on there.
We use the `%` character to delimit these two pieces of information.

**NOTE!** Channel names in IRC are always prefixed by `#`.

### Channels

To connect to IRC channels, the syntax is `#channel%irc.network.tld@irc.transport.tld`.
Here; '#channel' represents the IRC channel (eg. #fsf), 'irc.network.tld' represents the IRC network (eg. irc.freenode.net), and 'irc.transport.tld' represents IRC transport (eg. irc.disroot.org).

### Nicks

To connect to IRC channels, the syntax is `nick%irc.network.tld@irc.transport.tld`.
Here; 'nick' represents the IRC nick (eg. doe), 'irc.network.tld' represents the IRC network (eg. irc.oftc.net), and 'irc.transport.tld' represents IRC transport (eg. irc.snopyta.org).

## Authentication {#authentication}

After connection, if the network/channel requires you to login, you will receive messages from the IRC network (as irc.network.tld@irc.transport.tld) to do so.
You should reply to the IRC network in the form `NickServ IDENTIFY MyVeryLongPassword`, for authentication.
If authentication was successful, the server will tell you so.

**NOTE!** Unlike in Jabber, where authentication to chatrooms is performed by your Jabber server, authentication in IRC ecosystem is handled separately by every IRC network. This means if you're joining channels on multiple networks, you may need to authenticate separately on each of those networks.

## Settings {#settings}

There are three different levels of settings you may configure when using Biboumi, each with their own configuration address:

- IRC-Transport Level (per-transport settings) : `irc.transport.tld` (eg. irc.jabber.hot-chilli.org)
- IRC-Network Level (per-server settings) : `irc.network.tld@irc.transport.tld` (eg. irc.gimp.org@irc.jabber.hot-chilli.org)
- IRC-Channel Level (per-channel settings) : `#channel%irc.network.tld@irc.transport.tld` (eg. #gnome%irc.gimp.org@irc.jabber.hot-chilli.org)

**CAUTION!** Each of these settings level will override common settings in the previous one.

The settings are configured by what we call ad-hoc commands, supported by many clients.
In Gajim, you can right-click the element (eg. irc transport, irc network, or irc channel) you would like to configure, then click `Execute Command`.
This will open the corresponding settings panel.

### Automatic authentication {#auto-authentication}

It is possible to authenticate automatically on an IRC network.
You should configure authentication settings (username/password) on the IRC Network level by using ad-hoc commands as explained previously.
