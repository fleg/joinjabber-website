+++
title = "Welcome to Jabber"
+++

Welcome to the Jabber community. On this page, we'll help you find a server and a client to join the Jabber network and start chatting with your friends, without giving away all your data. Jabber is decentralized, just like email. This means you need to find a server, and from there you'll be able to chat with everyone else.

This may be confusing at first, but Jabber addresses (JIDs) look just like email addresses. So for example if you have the account `emma.goldman` on server jabber.fr, your address will be `emma.goldman@jabber.fr`. Chatrooms (also called MUCs) also have similar addresses, like `chat@joinjabber.org`.

Let's get started!

# Step 1: Register an account on a server

Depending on your needs, we can recommend different servers. Click on your use case to find out more.

<div class="usecases" style="text-align: center">
<details>
<summary>
{{ usecase(usecase="personal") }}
</summary>
<p>For your personal needs, you may be looking for a non-profit hosting provider with a sustainable economic model, that has more chances of being up and running 20 years from now. We recommend:</p>
{{ server(usecase="personal") }}
</details>
<details>
<summary>
{{ usecase(usecase="collective") }}
</summary>
<p>For your collective/organization, you may <a href="https://homebrewserver.club/category/instant-messaging.html">setup your own server</a>, for example with <a href="https://yunohost.org">Yunohost</a>. If that's outside your reach, we recommend the following professional hosting services:</p>
</details>
<details>
<summary>
{{ usecase(usecase="pseudo") }}
</summary>
<p>For your political activities, there are many servers providing pseudonymous Jabber services. Here's a few of them:</p>
{{ server(usecase="pseudo") }}
</details>
</div>



# Step 2: Pick a client

Now that you have found a server, you may download and configure a client. Here are the clients we recommend for their quality and their dedication to community needs. Click on your operating system to be taken to a dedicated documentation page.

<div class="platforms" style="text-align: center">

{{ platform(platform="android") }}
{{ platform(platform="ios") }}


{{ platform(platform="gnulinux") }}
{{ platform(platform="windows") }}
{{ platform(platform="macos") }}
</div>

# Step 3: You're in!

Congratulations! You have a server and a client. You can now open your client and give it your Jabber address to connect to the server and start chatting with some friends. Feeling lonely? You can join us in the community chat: [chat@joinjabber.org](xmpp:chat@joinjabber.org?join)
